import * as React from "react";

export type ThemePickerState = {
    theme?: any;
}

export default class ThemePicker extends React.Component<any, ThemePickerState> {
    constructor(props: any) {
        super(props);
        this.state = {
            theme: '',
        }
    }

    resetTheme = (evt: any) => {
        evt.preventDefault();
        this.setState({ theme: '' });
    }

    chooseTheme = (theme: any, evt: any) => {
        evt.preventDefault();
        this.setState({ theme });
    }

    render() {

        const { theme } = this.state;
        const themeClass = theme ? theme.toLowerCase() : 'secondary';

        return (
            <div className="d-flex flex-wrap justify-content-center position-absolute w-100 h-100 align-items-center align-content-center">
                <span className={`h1 mb-4 w-100 text-center text-${themeClass}`}>{theme || 'Default'}</span>
                <div className="btn-group">
                    <button type="button" className={`btn btn-${themeClass} btn-lg`}>{theme || 'Choose'} Theme</button>
                    <button type="button" className={`btn btn-${themeClass} btn-lg dropdown-toggle dropdown-toggle-split`} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span className="sr-only">Toggle Theme Dropdown</span>
                    </button>
                    <div className="dropdown-menu">
                        <a className="dropdown-item" href="#" onClick={this.chooseTheme.bind(this, 'Primary')}>Primary Theme</a>
                        <a className="dropdown-item" href="#" onClick={this.chooseTheme.bind(this, 'Danger')}>Danger Theme</a>
                        <a className="dropdown-item" href="#" onClick={this.chooseTheme.bind(this, 'Success')}>Success Theme</a>
                        <div className="dropdown-divider"></div>
                        <a className="dropdown-item" href="#" onClick={this.resetTheme.bind(this)}>Default Theme</a>
                    </div>

                </div>

            </div>
        );

    }
}