import React from 'react';
import './App.css';
import CVMainPageComponent from './Components/CVPage/CVMainPage';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PageNotFoundComponent from './Components/PageNotFoundComponent';
import MainPageComponent from './Components/MainPage/MainPage';
import LayoutComponent from './Components/Layout';

const App: React.FC = () => {
    return (
        <Router>
            <LayoutComponent>
                <Switch>
                    <Route exact path="/" component={MainPageComponent} />
                    <Route exact path="/cv" component={CVMainPageComponent} />
                    <Route component={PageNotFoundComponent} />
                </Switch>
            </LayoutComponent>
        </Router>
    );
}

export default App;
