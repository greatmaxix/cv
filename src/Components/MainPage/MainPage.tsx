import * as React from "react";
import tempAvatar from './../../Assets/BackgroundImages/tempAvatar.jpg';
import './MainPageStyle.css';
import LayoutContext from "../LayoutContext";
import SecondSection from "./SecondSection";

const MAIN_SECTION = 'main_section';
const SECOND_SECTION = 'second_section'; 

export default class MainPageComponent extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {

        }
    }

    private jumpTo(sectionName: string) {
        let sectionToJump = document.getElementById(sectionName);
        if (sectionToJump) {
            sectionToJump.scrollIntoView({
                behavior: 'smooth'
            });
        }
    }

    componentDidMount() {
        this.jumpTo(MAIN_SECTION);
    }

    render() {
        return (
            <>
            <div id="main_section" className="row main-section mx-0">
                <div className="row main-subsection mx-0">
                    <div className="col align-self-center">
                        <div className="main-avatar-frame  mx-auto">
                            <img src={tempAvatar} className="rounded-circle main-avatar mx-auto" alt="Avatar" />
                        </div>
                    </div>
                </div>
                <div className="row main-subsection mx-0">
                    <div className="col align-self-center text-center main-title-container">
                        <LayoutContext.Consumer>
                            {
                                value => (
                                    value ?
                                        <>
                                            <h1 className="main-title mx-auto">{value.textCollection.mainPageText.name}</h1>
                                            <p className="border-top main-title mx-auto"></p>
                                            <h4 className="main-title mx-auto">{value.textCollection.mainPageText.slogan}</h4>
                                        </>
                                        :
                                        <>
                                            <h1 className="main-title mx-auto">Maksat Baigazy</h1>
                                            <p className="border-top main-title mx-auto"></p>
                                            <h4 className="main-title mx-auto">Personal web-site of a young software developer</h4>
                                        </>
                                )
                            }
                        </LayoutContext.Consumer>
                        <div className="row">
                            <div className="col-sm-12 col-lg-12 col-md-12">
                                <div className="row">
                                    <div onClick={this.jumpTo.bind(this, SECOND_SECTION)} className="col align-self-center text-center">
                                        <i className="fa fa-angle-down fa-3x cursor-to-pointer" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="second_section">
                <SecondSection></SecondSection>
            </div>
            </>
        )
    }
}