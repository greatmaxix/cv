import * as React from "react";
import './MainPageSecondSection.css'

type SecondSectionState = {
    animationWasExecuted: boolean
}

export default class SecondSection extends React.Component<any, SecondSectionState> {

    constructor(props: any) {
        super(props);
        this.state = {
            animationWasExecuted: false
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.showUpWithAnimation);
    }

    showUpWithAnimation = () => {
        if (this.state && this.state.animationWasExecuted) return;
        let element = document.getElementById("card1");
        if (element) {
            const top = element.getBoundingClientRect().top;
            const offset = 0;
            if ((top + offset >= 0 && top - offset <= window.innerHeight)) {
                let cardCount = 3;
                for (let i = 0; i < cardCount; i++) {
                    let cardItem = document.getElementById("card" + (i + 1));
                    if (cardItem) {
                        cardItem.classList.add("fade-in");
                    }
                }
                this.setState({
                    animationWasExecuted: true
                })
            }
        }
    }

    onCardEnter(element) {
        if (element && element.currentTarget) {
            element.currentTarget.classList.add("card-mouse-over");
        }
    }

    onCardLeave(element) {
        if (element && element.currentTarget) {
            element.currentTarget.classList.remove("card-mouse-over");
            element.currentTarget.classList.remove("fade-in");
        }
    }

    render() {
        return (
            <>
                <div className="row mx-0 stars">
                    <div className="twinkling">
                        <div className="clouds">
                            <div id="cardsContainer" className="row heigh-100-vh justify-content-center align-items-center">
                                <div onMouseEnter={this.onCardEnter.bind(this)} onMouseLeave={this.onCardLeave.bind(this)} id="card1" className="col-lg-3 col-md-3 col-sm-12">
                                    <div className="card card-item">
                                        <div className="card-body ">
                                            <h3 className="card-title golden text-center">Complete websites</h3>
                                            <div className="card-text">
                                                <ul>
                                                    <li>
                                                        Illo ad eveniet quam consectetur veritatis nobis, dolorum totam voluptatibus asperiores voluptas provident.
                                                    </li>

                                                    <li>
                                                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Recusandae dicta eum quaerat veniam dolorem praesentium voluptates magni?
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="text-center">
                                                <a href="#" className="card-link">Contact me</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div onMouseEnter={this.onCardEnter.bind(this)} onMouseLeave={this.onCardLeave.bind(this)} id="card2" className="col-lg-3 col-md-3 col-sm-12">
                                    <div className="card card-item">
                                        <div className="card-body">
                                            <h3 className="card-title golden text-center">Web-page maintenance</h3>
                                            <div className="card-text">
                                                <ul>
                                                    <li>
                                                        Illo ad eveniet quam consectetur veritatis nobis, dolorum totam voluptatibus asperiores voluptas provident.
                                                    </li>

                                                    <li>
                                                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Recusandae dicta eum quaerat veniam dolorem praesentium voluptates magni?
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="text-center">
                                                <a href="#" className="card-link">Contact me</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div onMouseEnter={this.onCardEnter.bind(this)} onMouseLeave={this.onCardLeave.bind(this)} id="card3" className="col-lg-3 col-md-3 col-sm-12">
                                    <div className="card card-item">
                                        <div className="card-body">
                                            <h3 className="card-title golden text-center">Individual order</h3>
                                            <div className="card-text">
                                                <ul>
                                                    <li>
                                                        Illo ad eveniet quam consectetur veritatis nobis, dolorum totam voluptatibus asperiores voluptas provident.
                                                    </li>

                                                    <li>
                                                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Recusandae dicta eum quaerat veniam dolorem praesentium voluptates magni?
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="text-center">
                                                <a href="#" className="card-link">Contact me</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}