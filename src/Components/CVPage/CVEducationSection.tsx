import * as React from "react";
import './CVPageStyle.css';
import LayoutContext from "../LayoutContext";

export type EducationState = {
    schools: EducationObject[]
}

export type EducationObject = {
    fromDate: Date,
    toDate: Date,
    degreeTitle: string,
    schoolName: string,
    schoolLocation: string,
    educationDescription: string | null,
}


export default class CVEducationSection extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
        }
    }

    private decodeText(text: string | null) {
        const parser = new DOMParser();
        const decodedDescription = parser.parseFromString(`<!doctype html><body>${text}`, 'text/html').body.textContent;
        return decodedDescription;
    }

    private renderSchool(school: EducationObject, index?: number) {
        if (!index) index = 0;
        return (
            <div key={"Unique school key number" + index} className="container-fluid">
                <div className="row mx-0">
                    <div className="col-3 px-0">
                        <p>{school.fromDate + " - " + school.toDate}</p>
                    </div>
                    <div className="col-9 cv-main-content px-0">
                        <h4>{school.degreeTitle}</h4>
                        <p className="font-italic">{school.schoolName + " , " + school.schoolLocation}</p>
                        <p>{this.decodeText(school.educationDescription)}</p>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <LayoutContext.Consumer>
                {
                    value => (
                        value ?
                            <div id="work-history-section">
                                <br />
                                <div className="row mx-0">
                                    <div className="col px-0">
                                        <ul className="nav nav-tabs">
                                            <li><h3 className="cv-main-title font-weight-bold">{value.textCollection.commonText.content.education.title}</h3></li>
                                        </ul>
                                    </div>
                                </div>
                                <br />
                                {value.textCollection.cvPageText.EducationHistory.schools.map((school: EducationObject, index) => this.renderSchool(school, index))}
                            </div>
                        : "")
                }
            </LayoutContext.Consumer>
        );
    }
}