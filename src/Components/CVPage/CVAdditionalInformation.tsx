import * as React from "react";
import './CVPageStyle.css';
import LayoutContext from "../LayoutContext";

export type AdditionalInformationState = {
    mainInfo: string,
    interests: string[],
    hobbies: string,
}

export default class CVAdditionalInformation extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <LayoutContext.Consumer>
                {
                    value => (
                        value ?
                            <div id="additional-information-section">
                                <div id="main-info">
                                    <br />
                                    <div className="row mx-0">
                                        <div className="col px-0">
                                            <ul className="nav nav-tabs">
                                                <li><h3 className="cv-main-title font-weight-bold">{value.textCollection.commonText.content.additionalInformation.title}</h3></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <br />
                                    <div className="row mx-0">
                                        <div className="col-3 px-0">
                                        </div>
                                        <div className="col-9 cv-main-content px-0">
                                            <p>{value.textCollection.cvPageText.AdditionalInformation.mainInfo}</p>
                                        </div>
                                    </div>
                                </div>
                                <div id="interests">
                                    <br />
                                    <div className="row mx-0">
                                        <div className="col px-0">
                                            <ul className="nav nav-tabs">
                                                <li><h3 className="cv-main-title font-weight-bold">{value.textCollection.commonText.content.interests.title}</h3></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <br />
                                    <div className="row mx-0">
                                        <div className="col-3 px-0">
                                        </div>
                                        <div className="col-9 cv-main-content px-0">
                                            {value.textCollection.cvPageText.AdditionalInformation.interests.map((interest: string, index) => <p key={index}>{interest}</p>)}
                                        </div>
                                    </div>
                                </div>
                                <div id="hobbies">
                                    <br />
                                    <div className="row mx-0">
                                        <div className="col px-0">
                                            <ul className="nav nav-tabs">
                                                <li><h3 className="cv-main-title font-weight-bold">{value.textCollection.commonText.content.hobbies.title}</h3></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <br />
                                    <div className="row pb-5 mx-0">
                                        <div className="col-3 px-0">
                                        </div>
                                        <div className="col-9 cv-main-content px-0">
                                            <p>{value.textCollection.cvPageText.AdditionalInformation.hobbies}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            : "")
                }
            </LayoutContext.Consumer>
        );
    }
}

