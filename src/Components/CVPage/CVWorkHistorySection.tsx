import * as React from "react";
import './CVPageStyle.css';
import LayoutContext from "../LayoutContext";

export type WorkHistoryState = {
    Jobs: JobObject[]
}

export type JobObject = {
    fromDate: Date,
    toDate: Date,
    jobTitle: string,
    companyName: string,
    companyLocation: string,
    jobDescription: string | null,
}


export default class CVWorkHistorySection extends React.Component<any, any> {
    constructor(props: any) {
        
        super(props);
        this.state = {
        }
    }

    private decodeText(text: string | null) {
        const parser = new DOMParser();
        const decodedDescription = parser.parseFromString(`<!doctype html><body>${text}`, 'text/html').body.textContent;
        return decodedDescription;
    }

    private renderJob(job: JobObject, index?: number) {
        if (!index) index = 0;
        return (
            <div key={"Unique job key number" + index} className="container-fluid">
                <div className="row mx-0">
                    <div className="col-3 px-0">
                        <p>{job.fromDate + " - " + job.toDate}</p>
                    </div>
                    <div className="col-9 cv-main-content px-0">
                        <h4>{job.jobTitle}</h4>
                        <p className="font-italic">{job.companyName + " , " + job.companyLocation}</p>
                        <p>{this.decodeText(job.jobDescription)}</p>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <LayoutContext.Consumer>
                {
                    value => (
                        value ?
                            <div id="work-history-section">
                                <br/>
                                <div className="row mx-0">
                                    <div className="col px-0">
                                        <ul className="nav nav-tabs">
                                            <li><h3 className="cv-main-title font-weight-bold">{value.textCollection.commonText.content.workHistory.title}</h3></li>
                                        </ul>   
                                    </div>
                                </div>
                                <br/>
                                {value.textCollection.cvPageText.WorkHistory.Jobs.map((job: JobObject, index) => this.renderJob(job, index))}
                            </div>
                        : ""
                    )
                }
            </LayoutContext.Consumer>
        );
    }
}