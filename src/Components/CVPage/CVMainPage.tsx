import * as React from "react";
import CVQuickInfoComponent from "./CVQuickInfo";
import './CVPageStyle.css';
import CVWorkHistorySection from "./CVWorkHistorySection";
import CVEducationSection from "./CVEducationSection";
import CVAdditionalInformation from "./CVAdditionalInformation";

export type CVMainPageState = {
}

export default class CVMainPageComponent extends React.Component<any, CVMainPageState> {
    constructor(props: any) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <div className="row mx-0">
                <div className="col-lg-3 col-sm-12 col-md-12 px-0">
                    <CVQuickInfoComponent></CVQuickInfoComponent>
                </div>
                <div className="col-lg-9 col-sm-12 col-md-12 px-0">
                    <CVWorkHistorySection></CVWorkHistorySection>
                    <CVEducationSection></CVEducationSection>
                    <CVAdditionalInformation></CVAdditionalInformation>
                </div>
            </div>
        );
    }
}