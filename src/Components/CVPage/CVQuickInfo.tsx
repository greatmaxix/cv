import * as React from "react";
import './CVPageStyle.css';
import CSS from 'csstype';
import LayoutContext from "../LayoutContext";


export type CVQuickInfoState = {
    birthdate: Date
    address: string,
    phone: string,
    email: string,
    skills: CVSkillObject[],
    languages: CVSkillObject[]
}

export type CVSkillObject = {
    skillName: string,
    skillPoint: CVSkillPoint,
}

export enum CVSkillPoint {
    PoorKnowledge,
    BasicKnowledge,
    SatisfactoryKnowledge,
    GoodKnowledge,
    ExcellentKnowledge,
}

export default class CVQuickInfoComponent extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {

        }
    }

    private skillRender(skill: CVSkillObject, index?: number) {
        if (!index) index = 228;
        let cssWidth: CSS.Properties = { width: ` ` + Math.round((skill.skillPoint + 1) * 100 / 5) + `%` };
        return (
            <div key={"Unique key number" + index} className="mt-2 mb-2">
                <div className="row mx-0">
                    <div className="col px-0">
                        <label className="cv-quick-info-full-width">
                            {skill.skillName}
                            <div className="progress">
                                <div className="progress-bar" role="progressbar" aria-valuenow={70}
                                    aria-valuemin={0} aria-valuemax={100} style={cssWidth}>
                                </div>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        return (
            <LayoutContext.Consumer>
                {
                    value => (
                        value ?
                            <div id="cv_info_container" className="cv-quick-info-container">
                                <h2 className="mt-3 mb-3 font-weight-bold">{value.textCollection.commonText.content.resume.title}</h2>
                                <p>{value.textCollection.commonText.content.resume.birthText + value.textCollection.cvPageText.QuickInfoText.birthdate}</p>
                                <div className="cv-quick-info-header mt-4 mb-4">
                                    <h3 className="mt-3 mb-3 font-weight-bold">{value.textCollection.commonText.content.contact.title}</h3>
                                </div>
                                <p className="font-weight-bold mb-1">{value.textCollection.commonText.content.contact.address}</p>
                                <p>{value.textCollection.cvPageText.QuickInfoText.address}</p>
                                <p className="font-weight-bold mb-1">{value.textCollection.commonText.content.contact.phone}</p>
                                <a className="text-decoration-none" href={"tel:" + value.textCollection.cvPageText.QuickInfoText.phone}>{value.textCollection.cvPageText.QuickInfoText.phone}</a>
                                <p className="font-weight-bold mb-1 mt-2">{value.textCollection.commonText.content.contact.email}</p>
                                <a className="text-decoration-none" href={"mailto:" + value.textCollection.cvPageText.QuickInfoText.email}>{value.textCollection.cvPageText.QuickInfoText.email}</a>
                                <div className="cv-quick-info-header mt-4 mb-4">
                                    <h3 className="mt-3 mb-3 font-weight-bold">{value.textCollection.commonText.content.skills.title}</h3>
                                </div>
                                {value.textCollection.cvPageText.QuickInfoText.skills.map((currSkill: CVSkillObject, index) => this.skillRender(currSkill, index))}
                                <div className="cv-quick-info-header mt-4 mb-4">
                                    <h3 className="mt-3 mb-3 font-weight-bold">{value.textCollection.commonText.content.languages.title}</h3>
                                </div>
                                {value.textCollection.cvPageText.QuickInfoText.languages.map((currLangSkill: CVSkillObject, index) => this.skillRender(currLangSkill, index))}
                            </div>
                            : ""
                    )
                }
            </LayoutContext.Consumer>
        );
    }
}