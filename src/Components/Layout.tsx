import * as React from "react";
import axios from 'axios';
import FooterComponent from "./Footer/Footer";
import NavbarComponent from "./Navbar/Navbar";
import { LayoutContext } from "./LayoutContext";

type LayoutComponentState = {
    translations: any,
    languageValue: string | null,
}

export default class LayoutComponent extends React.Component<any, LayoutComponentState> {
    constructor(props: any) {
        super(props);
        this.state = {
            translations: undefined,
            languageValue: null
        }
    }

    componentDidMount() {
        this.reloadAsync();
    }

    private async reloadAsync() {
        await this.getTranslations(null);
    }

    private async getTranslations(languageValueCode: string | null) {
        let data: any = undefined;
        const userLang = languageValueCode ? languageValueCode : navigator.language;
        try {
            await axios.get('http://localhost:8080/api/translations', { params: { langcode: userLang } })
                .then((response) => { 
                    this.setState({translations: response.data}) 
                })
                .catch((error) => {console.error(error)})
        }
        catch (error) {
            console.error(error.message)
        }
        return data;
    }

    private async updateLanguageValue(value: any) {
        this.setState({languageValue: value});
        await this.getTranslations(value);
    }

    render() {
        return (
            <LayoutContext.Provider value={this.state.translations}>
                <NavbarComponent onLanguageUpdate={this.updateLanguageValue.bind(this)}/>
                    <section>
                        <div className="container-fluid px-0">
                            {this.props.children}
                        </div>
                    </section>
                <FooterComponent/>
            </LayoutContext.Provider>
        )
    }
}