import React from 'react';
import { CVSkillObject } from './CVPage/CVQuickInfo';
import { JobObject } from './CVPage/CVWorkHistorySection';
import { EducationObject } from './CVPage/CVEducationSection';
export type ContextValueType = {
    textCollection: {
        commonText: {
            name: string,
            tabs: LayoutTabElem[],
            footer: {
                links: string
            },
            content: {
                resume: {
                    title: string,
                    birthText: string,
                },
                contact: {
                    title: string,
                    address: string,
                    phone: string,
                    email: string
                },
                skills: { title: string },
                languages: { title: string },
                workHistory: { title: string },
                education: { title: string },
                additionalInformation: { title: string },
                interests: { title: string },
                hobbies: { title: string },
            }
        },
        cvPageText: {
            QuickInfoText: {
                birthdate: Date,
                address: string,
                phone: string,
                email: string,
                skills: CVSkillObject[],
                languages: CVSkillObject[]
            },
            WorkHistory: {
                Jobs: JobObject[]
            },
            EducationHistory: {
                schools: EducationObject[]
            },
            AdditionalInformation: {
                mainInfo: string,
                interests: string[],
                hobbies: string
            }
        },
        mainPageText: {
            name: string,
            slogan: string
        }
    }
}

export type LayoutTabElem = {
    url: string,
    tabName: string
}

export const LayoutContext = React.createContext<ContextValueType | undefined>(undefined);
export default LayoutContext;