import * as React from "react";
import LayoutContext from "../LayoutContext";

export type FooterState = {
}

export default class FooterComponent extends React.Component<any, FooterState> {
    constructor(props: any) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <footer className="pb-5 pt-5 border-top">
                <div className="row mx-0">
                    <div className="col-sm-12 col-md-6 col-lg-6 text-center">
                        <small className="d-block mb-3 text-muted">© 2020</small>
                    </div>
                    <div className="col-sm-12 col-md-6 col-lg-6 text-center">
                        <LayoutContext.Consumer>
                            {
                                value => (
                                    value ?
                                    <h5>{value.textCollection.commonText.footer.links}</h5>
                                    : ""
                                )
                            }
                        </LayoutContext.Consumer>
                        <ul className="list-unstyled text-small">
                            <li><a className="text-muted" href="https://gitlab.com/greatmaxix">GitLab</a></li>
                            <li><a className="text-muted" href="https://www.linkedin.com/in/maksat-baigazy-999527162/">LinkedIn</a></li>
                            <li><a className="text-muted" href="https://www.facebook.com/maksat.baigazy">Facebook</a></li>
                        </ul>
                    </div>
                </div>
            </footer>
        );
    }
}