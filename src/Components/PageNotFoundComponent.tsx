import * as React from "react";
import { Link } from "react-router-dom";

export default class PageNotFoundComponent extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {

        }
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="jumbotron">
                    <h1 className="display-4">Page not found!</h1>
                    <p className="lead">Probably you have navigated to a wrong page.</p>
                    <hr className="my-4" />
                    <p className="lead">
                        <Link to='/' className="btn btn-primary btn-lg" role="button">Go back</Link>
                    </p>
                </div>
            </div>
        )
    }
}