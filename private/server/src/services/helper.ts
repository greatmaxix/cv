export default class Helper {
    public static getProperLangCode(langcode: string | undefined) {
        const properCodes : string[] = [
            'ru-RU',
            'en-EN'
        ];
        let foundCode = properCodes.find(el => {
            if (langcode && el.includes(langcode)) {
                return el;
            }
            else {
                return el === 'en-EN';
            }
        });

        return foundCode;
    }
}