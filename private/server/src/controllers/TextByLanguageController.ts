// import { db } from "../database/mongoDb";
import ErrorHandler from "../models/ErrorHandler";
import aRepository from "../repositories/aRepository";
import TextByLanguageRepository from "../repositories/TextByLanguageRepository";


export class TextByLanguageController {
    private repository: aRepository;

    constructor() {
        this.repository = new TextByLanguageRepository();
    }

    defaultMethod() {
        return {
          text: `You've reached the ${this.constructor.name} default method`
        };
    }

    someErrorMethod() {
        throw new ErrorHandler(501, 'Not implemented method');
    }
}