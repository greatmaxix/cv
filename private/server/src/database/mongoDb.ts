import { Db, MongoClient, MongoError } from 'mongodb';

export default class mongoDbClient {
    public client: MongoClient | null = null;

    public connect(url: string, callback: (...[]) => any ) {
        if (this.client == null) {
            this.client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true });
            this.client.connect((err: MongoError) => {
                if (err) {
                    this.client = null;
                    callback(err);
                } else {
                    callback();
                }
            });
        } else {
            callback();
        }
    }

    public db(dbName: string) : Db {
        if (this.client) {
            return this.client.db(dbName);
        }
        else {
            throw new Error("Estabilish connection first!");
        }
    }

    public close() {
        if (this.client) {
            this.client.close();
            this.client = null;
        }
    }
}