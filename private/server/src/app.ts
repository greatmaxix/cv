import dotenv from 'dotenv';
import express, { Request, Response, NextFunction, Router } from 'express';
import MasterRouter from './routers/MasterRouter';
import ErrorHandler from './models/ErrorHandler';
import mongoDbClient from './database/mongoDb'
import cors from "cors";

// load the environment variables from the .env file
dotenv.config({
    path: '.env'
});

let mRouter;
let dbClient = new mongoDbClient();
/**
 * Express server application class.
 * @description Will later contain the routing system.
 */
class Server {
    public app = express();
    public router = MasterRouter;
}
const server = new Server();

// initialize server app 
const uri = "mongodb+srv://" + process.env.MONGO_DB_USR_NAME + ":" + process.env.MONGO_DB_USR_PWD + "@cluster0-5o6nh.mongodb.net/" + process.env.MONGO_DB_NAME + "?retryWrites=true&w=majority";

dbClient.connect(uri, (error) => {
    if (error) {
        console.error('Unable to connect to Mongo', error);
        process.exit();
    }
    else {
        ((port = process.env.APP_PORT || 5000) => {
            server.app.listen(port, () => console.log(`> Listening on port ${port}`));
        })();
    }
});

server.app.locals.dbClient = dbClient;

server.app.use(cors());

server.app.use('/api', server.router);

server.app.use((err: ErrorHandler, req: Request, res: Response, next: NextFunction) => {
    res.status(err.statusCode || 500).json({
        status: 'error',
        statusCode: err.statusCode,
        message: err.message
    });
});