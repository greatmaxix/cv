import { Collection } from "mongodb";
import aRepository from "./aRepository";

export default class TextByLanguageRepository extends aRepository {
    static COLLECTION = 'TextByLanguage';
    constructor() {
        super();
    }
}