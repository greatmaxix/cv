import { Router } from 'express';
import { TextByLanguageRouter } from './TextByLanguageRouter';

class MasterRouter {
    private _router = Router();

    get router() {
        return this._router;
    }
  
    constructor() {
        this._configure();
    }

    private _textByLanguage = new TextByLanguageRouter().router;;
  
    private _configure() {
        this._router.use('/translations', this._textByLanguage);
    }
}

export default new MasterRouter().router;