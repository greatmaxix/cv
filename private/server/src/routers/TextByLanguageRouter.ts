import { NextFunction, Request, Response, Router } from 'express';
import { TextByLanguageController } from '../controllers/TextByLanguageController';
import mongoDbClient from '../database/mongoDb';
import url from 'url';
import Helper from '../services/helper';

export class TextByLanguageRouter {
    private _router = Router();
    private _controller = new TextByLanguageController();

    get router() {
        return this._router;
    }

    constructor() {
        this._configure();
    }

    private _configure() {
        this._router.get('/', (req: Request, res: Response, next: NextFunction) => {
            const dbClient: mongoDbClient = req.app.locals.dbClient;
            let collection = dbClient.db(process.env.MONGO_DB_NAME!).collection('TextByLanguage');
            let urlParams = url.parse(req.url, true).query;
            
            let langCode = Helper.getProperLangCode(urlParams.langcode && !Array.isArray(urlParams.langcode) ? urlParams.langcode : undefined);
            let json = null;
            collection.findOne({"language": langCode}, (err, TextByLanguage) => {
                
                if(err) return console.log(err);
                if (!TextByLanguage) {
                    collection.findOne({"language": "en-EN"}, (err, DefaultText) => {
                        if (err) return console.log(err);
                        res.status(200).json(
                            DefaultText
                        )
                    });
                }
                else {
                    res.status(200).json(
                        TextByLanguage
                    )
                    json = TextByLanguage;
                }
            })
            
                    
        });

        this._router.get('/error', (req: Request, res: Response, next: NextFunction) => {
            try {
                
                const result = this._controller.someErrorMethod();
                res.status(200).json(result);
            }
            catch (error) {
                next(error);
            }
        });
    }
}